class RemoveStaticPages < ActiveRecord::Migration[5.1]
  def change
    drop_table :static_pages
  end
end
